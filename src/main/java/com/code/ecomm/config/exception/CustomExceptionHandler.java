package com.code.ecomm.config.exception;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(Exception.class)
    public String handleUsernameNotFoundException(Exception exception) {
        return "INVALID CREDENTIALS" +" __ "+ exception.getMessage();
    }

}
