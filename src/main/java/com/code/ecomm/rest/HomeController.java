package com.code.ecomm.rest;

import com.code.ecomm.model.AuthRequest;
import com.code.ecomm.model.AuthResponse;
import com.code.ecomm.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

@RestController
public class HomeController {

    private AuthenticationManager authenticationManager;
    private UserDetailsService userDetailsService;

    @Autowired
    public HomeController(AuthenticationManager authenticationManager, UserDetailsService userDetailsService) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
    }

    @GetMapping
    public String home() {
        return ("<h1> Welcome </h1>");
    }

    @GetMapping("/admin")
    public String admin() {
        return ("<h1> Welcome ADMIN</h1>");
    }

    @GetMapping("/user")
    public String user() {
        return ("<h1> Welcome USER</h1>");
    }

    @PostMapping("/authenticate")
    public ResponseEntity<AuthResponse> authenticate(@RequestBody AuthRequest authRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword())
            );
        } catch (BadCredentialsException e) {
            System.out.println("BAD CREDENTIALS");
            throw new Exception("BAD CREDENTIALS PROVIDED");
        }
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authRequest.getUsername());
        AuthResponse response = new AuthResponse(new JwtUtil().generateToken(userDetails));
        return ResponseEntity.ok(response);
    }
    
//    @GetMapping("/login")
//    public String welcome() {
//        return "Dhiren";
//    }

}
